# multiple-cache-boot-starter

#### 介绍
基于Spring Cache + Caffeine + Redis的多级缓存管理组件，完全兼容@Caching、@Cacheable、@CacahePut、@CacheEvict等注解。同时支持自定义缓存API层级的缓存策略，以及通过Redis订阅发布机制，实现缓存一致性。


#### 软件架构
- JDK1.8
- SpringBoot 2.x
- Caffeine 2.8.8
- Redisson 3.8.2


#### 使用说明

###### 1.  引入依赖

```
<!--引入多级缓存组件-->
<dependency>
    <groupId>com.ryan.multiple.cache</groupId>
    <artifactId>multiple-cache-boot-starter</artifactId>
    <version>${multiple-cache.version}</version>
</dependency>
```

###### 2.  默认配置项
在未对具体API进行缓存策略配置时，该配置项作为多级缓存默认全局配置。

```
## 默认配置
### 缓存类型：caffeine-一级缓存，redis-二级缓存，multiple-多级缓存
ryan.multiple.cache.default.type=caffeine
### 缓存参数存活时间（单位：秒）
ryan.multiple.cache.default.ttl=1800
### 缓存最大空闲时间（单位：秒）
ryan.multiple.cache.default.maxIdleTime=1800
### 缓存最大记录数
ryan.multiple.cache.default.maxSize=1000
### 是否允许缓存Null值
ryan.multiple.cache.default.allowNullValues=true

```

###### 3.  自定义配置项
支持API层级的缓存配置管理，对不同API进行定制化配置。如下示例标识对缓存名称为 sayHello 的API缓存配置。

```
## 默认配置
### 缓存类型：caffeine-一级缓存，redis-二级缓存，multiple-多级缓存
ryan.multiple.cache.sayHello.type=multiple
### 缓存参数存活时间（单位：秒）
ryan.multiple.cache.sayHello.ttl=300
### 缓存最大空闲时间（单位：秒）
ryan.multiple.cache.sayHello.maxIdleTime=300
### 缓存最大记录数
ryan.multiple.cache.sayHello.maxSize=100
### 是否允许缓存Null值
ryan.multiple.cache.sayHello.allowNullValues=true

```


###### 4.  缓存管理示例

- @Cacheable
```
@GetMapping("/get")
@Cacheable(cacheNames = "sayHello", key = "#userName")
public String getSayHello(String userName) {
    LOGGER.info("Cacheable --> keyName={}.", userName);
    return "Hello, " + userName + "!";
}

```


- @CachePut
```
@PutMapping("/put")
@CachePut(cacheNames = "sayHello", key = "#userName")
public String putSayHello(String userName) {
    LOGGER.info("CachePut --> keyName={}.", userName);
    return "Hello, " + userName + "!";
}
```


- @CacheEvict
```
@DeleteMapping("/evict")
@CacheEvict(cacheNames = "sayHello", key = "#userName")
public void evictSayHello(String userName) {
    LOGGER.info("CacheEvict --> keyName={}.", userName);
}
```


###### 5.  Redis配置
当前组件中已经集成了Redis的客户端，支持单机模式、主从模式、哨兵模式和Cluster集群模式，只需要在配置文件中指定Redis服务部署模式，并在对应模式下配置redis节点信息。具体参考如下：

- 单机模式
```
ryan.redis.mode=single
ryan.redis.single.address=127.0.0.1:6379
ryan.redis.single.password=cache@12345
```

- 主从模式
```
ryan.redis.mode=masterSlave
ryan.redis.masterSlave.master=127.0.0.1:6379
ryan.redis.masterSlave.nodes=127.0.0.1:6380,127.0.0.1:6381
ryan.redis.masterSlave.password=cache@12345
```


- 哨兵模式
```
ryan.redis.mode=sentinel
ryan.redis.masterSlave.master=127.0.0.1:6379
ryan.redis.masterSlave.nodes=127.0.0.1:6380,127.0.0.1:6381
ryan.redis.masterSlave.password=cache@12345
```


- 集群模式
```
ryan.redis.mode=cluster
ryan.redis.masterSlave.nodes=127.0.0.1:6379,127.0.0.1:6380,127.0.0.1:6381
ryan.redis.masterSlave.password=cache@12345
```

除了上述基本配置信息项，其他相关参数均使用了redisson客户端中默认参数。
如果要对默认参数进行修改，则需要在项目中实现如下接口即可：
> 注意：RedisServerConfigHandler接口的对应实现类需要注入到spring容器中。
```
public interface RedisServerConfigHandler<T> {

    void fillConfigs(T config);

}
```

> 如下示例：调整重试次数为5次，此时默认参数retryAttempts=3会被覆盖。

```
@Component
public class RedisSingleServerConfigHandler implements RedisServerConfigHandler<SingleServerConfig> {

    @Override
    public void fillConfigs(SingleServerConfig config) {
        // 设置重试次数为5次
        config.setRetryAttempts(5);
    }

}
```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
