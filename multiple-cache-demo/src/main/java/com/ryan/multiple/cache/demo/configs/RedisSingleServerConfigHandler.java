package com.ryan.multiple.cache.demo.configs;

import com.ryan.multiple.cache.redis.RedisServerConfigHandler;
import org.redisson.config.SingleServerConfig;
import org.springframework.stereotype.Component;

@Component
public class RedisSingleServerConfigHandler implements RedisServerConfigHandler<SingleServerConfig> {

    @Override
    public void fillConfigs(SingleServerConfig config) {
        // 设置重试次数为5次
        config.setRetryAttempts(5);
    }

}
