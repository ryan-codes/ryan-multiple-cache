/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.core;

import com.github.benmanes.caffeine.cache.RemovalCause;
import com.github.benmanes.caffeine.cache.RemovalListener;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * .缓存清理监听器类
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
public class MultipleCacheRemovalListener implements RemovalListener<Object, Object> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MultipleCacheRemovalListener.class);

    private final MultipleCacheNotifier notifier;
    public MultipleCacheRemovalListener(MultipleCacheNotifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public void onRemoval(@Nullable Object key, @Nullable Object value, @NonNull RemovalCause cause) {
        LOGGER.info("Cacheable {} ---> key={}, value={}.", cause, key, value);
        // 一级缓存被回收或过期时处理
        if(cause.equals(RemovalCause.COLLECTED) || cause.equals(RemovalCause.EXPIRED)) {
            // 当对象被回收或过期时需要处理
            return;
        }
        // 缓存更新时处理
        if(cause.equals(RemovalCause.REPLACED)) {
            return;
        }
    }
}
