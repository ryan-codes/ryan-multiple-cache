/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.consts;

import java.util.stream.Stream;

/**
 * .缓存类型枚举类
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
public enum CacheType {

    /**
     * .caffeine：本地一级缓存
     */
    CAFFEINE,

    /**
     * .redis：redis二级缓存
     */
    REDIS,

    /**
     * .multiple：多级缓存
     */
    MULTIPLE;

    public static final CacheType of(String type) {
        return Stream.of(values()).filter(e -> e.name().equalsIgnoreCase(type))
                    .findFirst().orElseThrow(() -> new IllegalArgumentException("Unable to match suitable cache type: " + type));
    }

}
