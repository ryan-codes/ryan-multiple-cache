/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.core;

import com.ryan.multiple.cache.consts.CacheType;
import com.ryan.multiple.cache.event.CacheEvent;
import com.ryan.multiple.cache.properties.CacheProperties;
import org.redisson.spring.cache.RedissonCache;
import org.springframework.cache.Cache;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.AbstractValueAdaptingCache;

import java.util.Objects;
import java.util.concurrent.Callable;

/**
 * .多级缓存实现类
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
public class MultipleCache extends AbstractValueAdaptingCache {

    private final CacheProperties properties;
    private final Cache caffeineCache;
    private final Cache redissonCache;
    private final MultipleCacheNotifier notifier;
    protected MultipleCache(CacheProperties properties, Cache caffeineCache, Cache redissonCache, MultipleCacheNotifier notifier) {
        super(properties.getAllowNullValues());
        this.properties= properties;
        this.caffeineCache = caffeineCache;
        this.redissonCache = redissonCache;
        this.notifier = notifier;
    }

    private boolean isCaffeinePresent() {
        return Objects.nonNull(caffeineCache);
    }

    private boolean isRedissonPresent() {
        return Objects.nonNull(redissonCache);
    }

    private boolean isNeedPublish() {
        return CacheType.MULTIPLE.name().equalsIgnoreCase(properties.getType());
    }

    private <T> T safeCast(Object value) {
        if (Objects.isNull(value) || Objects.isNull(fromStoreValue(value))) {
            return null;
        }

        return (T) fromStoreValue(value);
    }

    @Override
    protected Object lookup(Object key) {
        // 如果一级缓存存在，则直接返回一级缓存数据
        if (isCaffeinePresent()) {
            ValueWrapper valueWrapper = caffeineCache.get(key);
            if (Objects.nonNull(valueWrapper)) {
                return safeCast(valueWrapper.get());
            }
        }
        // 一级缓存不存在时，则查询二级缓存
        if (isRedissonPresent()) {
            ValueWrapper valueWrapper = redissonCache.get(key);
            // 二级缓存存在时，返回前先将缓存数据同步到一级缓存
            if (Objects.nonNull(valueWrapper)) {
                if (isCaffeinePresent()) {
                    caffeineCache.put(key, toStoreValue(valueWrapper.get()));
                }
                return safeCast(valueWrapper.get());
            }
        }

        return safeCast(null);
    }

    @Override
    public String getName() {
        return this.properties.getName();
    }

    @Override
    public Object getNativeCache() {
        return this;
    }

    @Override
    public <T> T get(Object key, Callable<T> valueLoader) {
        if (isRedissonPresent()) {
            RedissonCache realRedissonCache = (RedissonCache) redissonCache;
            // 利用Redisson缓存,已经实现了get和put操作
            Object newValue = realRedissonCache.get(key, valueLoader);
            if (isCaffeinePresent()) {
                // 如果Caffeine也存在，则同步到Caffeine缓存
                caffeineCache.put(key, toStoreValue(newValue));
            }
            return safeCast(newValue);
        }

        if (isCaffeinePresent()) {
            // 仅使用Caffeine缓存
            CaffeineCache realCaffeineCache = (CaffeineCache) caffeineCache;
            Object newValue = realCaffeineCache.get(key, valueLoader);
            return safeCast(newValue);
        }

        Object value = null;
        try {
            value = valueLoader.call();
        } catch (Exception e) {
            throw new RuntimeException("Invoke target method failed. ", e);
        }

        return safeCast(value);
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        this.put(key, value);
        return toValueWrapper(value);
    }

    @Override
    public void put(Object key, Object value) {
        if (isRedissonPresent()) {
            redissonCache.put(key, toStoreValue(value));
            if (isNeedPublish()) {
                notifier.publish(CacheEvent.PUT, this.getName(), key, value);
            }
        }

        if (isCaffeinePresent()) {
            caffeineCache.put(key, toStoreValue(value));
        }
    }

    @Override
    public void evict(Object key) {
        if (isRedissonPresent()) {
            redissonCache.evict(key);
            if (isNeedPublish()) {
                notifier.publish(CacheEvent.EVICT, this.getName(), key, null);
            }
        }

        if (isCaffeinePresent()) {
            caffeineCache.evict(key);
        }
    }

    @Override
    public void clear() {
        if (isRedissonPresent()) {
            redissonCache.clear();
            if (isNeedPublish()) {
                notifier.publish(CacheEvent.CLEAR, this.getName(), null, null);
            }
        }

        if (isCaffeinePresent()) {
            caffeineCache.clear();
        }
    }
}
