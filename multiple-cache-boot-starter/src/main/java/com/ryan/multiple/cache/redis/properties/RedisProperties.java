/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.redis.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * .Redis客户端配置类
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = "ryan.redis")
public class RedisProperties {

    /**
     * .redis模式
     */
    private String mode;

    /**
     * 单机模式信息配置
     */
    private SingleProperties single;

    /**
     * 主从模式信息配置
     */
    private MasterSlaveProperties masterSlave;

    /**
     * 哨兵模式信息配置
     */
    private SentinelProperties sentinel;

    /**
     * 集群模式信息配置
     */
    private ClusterProperties cluster;

    /**
     * 分布式锁配置
     */
    private LockProperties lock;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public SingleProperties getSingle() {
        return single;
    }

    public void setSingle(SingleProperties single) {
        this.single = single;
    }

    public MasterSlaveProperties getMasterSlave() {
        return masterSlave;
    }

    public void setMasterSlave(MasterSlaveProperties masterSlave) {
        this.masterSlave = masterSlave;
    }

    public SentinelProperties getSentinel() {
        return sentinel;
    }

    public void setSentinel(SentinelProperties sentinel) {
        this.sentinel = sentinel;
    }

    public ClusterProperties getCluster() {
        return cluster;
    }

    public void setCluster(ClusterProperties cluster) {
        this.cluster = cluster;
    }

    public LockProperties getLock() {
        return lock;
    }

    public void setLock(LockProperties lock) {
        this.lock = lock;
    }

}
