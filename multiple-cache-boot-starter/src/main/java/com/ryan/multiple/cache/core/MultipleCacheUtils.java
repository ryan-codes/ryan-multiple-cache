package com.ryan.multiple.cache.core;

import com.ryan.multiple.cache.properties.CacheProperties;
import org.redisson.spring.cache.CacheConfig;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * .多级缓存工具类
 *
 * @author Ryanyu2016
 * @since 1.0.1
 */
public final class MultipleCacheUtils {

    public static CacheConfig getCacheConfig(CacheProperties properties) {
        CacheConfig cacheConfig = new CacheConfig();
        if (Objects.nonNull(properties.getTtl())) {
            cacheConfig.setTTL(TimeUnit.SECONDS.toMillis(properties.getTtl()));
        }
        if (Objects.nonNull(properties.getMaxIdleTime())) {
            cacheConfig.setMaxIdleTime(TimeUnit.SECONDS.toMillis(properties.getMaxIdleTime()));
        }
        if (Objects.nonNull(properties.getMaxSize())) {
            cacheConfig.setMaxSize(properties.getMaxSize());
        }
        return cacheConfig;
    }

}
