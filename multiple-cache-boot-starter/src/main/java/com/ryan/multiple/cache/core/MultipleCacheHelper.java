/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.core;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.ryan.multiple.cache.consts.CacheType;
import com.ryan.multiple.cache.consts.CacheValueType;
import com.ryan.multiple.cache.properties.CacheProperties;
import com.ryan.multiple.cache.redis.RedisNamespaceHandler;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.caffeine.CaffeineCache;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * .多级缓存工具类
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
public class MultipleCacheHelper {

    /**
     * .日志对象
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MultipleCacheHelper.class);

    private RedissonClient redissonClient;
    private RedisNamespaceHandler redisNamespaceHandler;

    private MultipleCacheNotifier cacheNotifier;
    private MultipleCacheRemovalListener cacheRemovalListener;

    public MultipleCacheHelper setRedissonClient(RedissonClient redissonClient) {
        this.redissonClient = redissonClient;
        return this;
    }

    public MultipleCacheHelper setNamespaceHandler(RedisNamespaceHandler redisNamespaceHandler) {
        this.redisNamespaceHandler = redisNamespaceHandler;
        return this;
    }

    public MultipleCacheHelper setCacheNotifier(MultipleCacheNotifier cacheNotifier) {
        this.cacheNotifier = cacheNotifier;
        return this;
    }

    public MultipleCacheHelper setRemovalListener(MultipleCacheRemovalListener cacheRemovalListener) {
        this.cacheRemovalListener = cacheRemovalListener;
        return this;
    }

    private String getNamespace() {
        // todo Redis缓存命名空间，应用侧也可以自行扩展该命名空间
        return Objects.isNull(redisNamespaceHandler) ? "multiple-cache"
                : redisNamespaceHandler.getNamespace();
    }

    private String getNamespaceName(String originalName) {
        return String.format("%s::%s", this.getNamespace(), originalName);
    }

    public Cache buildCaffeineCache(CacheProperties properties) {
        Caffeine<Object, Object> caffeine = Caffeine.newBuilder()
                .maximumSize(properties.getMaxSize())
                // 最后一次写入或者访问后多久过期
                .expireAfterAccess(properties.getMaxIdleTime(), TimeUnit.SECONDS)
                // 设置监听，当出现自动删除时的回调
                .removalListener(cacheRemovalListener);
        LOGGER.info("Build caffeine cache '{}' : {}", properties.getName(), caffeine.toString());
        return new CaffeineCache(properties.getName(), caffeine.build(), properties.getAllowNullValues());
    }

    public Cache buildRedissonCache(CacheProperties properties) {
        // 缓存对象名称
        String cacheName = getNamespaceName(properties.getName());
        // 数据结构类型适配
        String valueType = properties.getValueType();
        return  CacheValueType.of(valueType).getCache(cacheName, redissonClient, properties);
    }

    public MultipleCache buildMultipleCache(CacheProperties properties) {
        Cache caffeineCache = null;
        Cache redissonCache = null;
        final CacheType cacheType = CacheType.of(properties.getType());
        if (CacheType.CAFFEINE == cacheType || CacheType.MULTIPLE == cacheType) {
            caffeineCache = buildCaffeineCache(properties);
        }
        if (CacheType.REDIS == cacheType || CacheType.MULTIPLE == cacheType) {
            redissonCache = buildRedissonCache(properties);
        }

        return new MultipleCache(properties, caffeineCache, redissonCache, cacheNotifier);
    }

}
