/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.core;

import com.ryan.multiple.cache.event.CacheEvent;
import com.ryan.multiple.cache.event.CacheEventArgs;
import org.redisson.api.RTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * .多级缓存通知
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
public class MultipleCacheNotifier {

    /**
     * .日志对象
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MultipleCacheNotifier.class);

    public final RTopic topic;
    public MultipleCacheNotifier(RTopic topic) {
        this.topic = topic;
    }

    public MultipleCacheNotifier() {
        this.topic = null;
    }

    public void publish(CacheEvent command, String name, Object key, Object value) {
        CacheEventArgs message = new CacheEventArgs.Builder()
                .command(command).cacheName(name).cacheKey(key).cacheValue(value)
                .build();

        long received = 0L;
        try {
            received = topic.publish(message);
        } finally {
            LOGGER.info("-----> push cache message={}, received={}.", message, received);
        }
    }


}
