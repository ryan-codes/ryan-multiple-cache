/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.datatype;

import com.ryan.multiple.cache.properties.CacheProperties;
import org.redisson.api.RBucket;
import org.redisson.api.RLock;
import org.springframework.cache.Cache;

import java.util.concurrent.Callable;

/**
 * .Redis String 数据结构缓存实现类
 *
 * @author Ryanyu2016
 * @since 1.0.1
 */
public class RedissonStringCache extends BaseCacheConfig implements Cache {

    private final String cacheName;
    private final RBucket<Object> bucketCache;
    private final CacheProperties properties;
    private final RLock redisLock;

    public RedissonStringCache(String cacheName, RBucket<Object> bucketCache, RLock redisLock, CacheProperties properties) {
        super(properties);
        this.cacheName = cacheName;
        this.bucketCache = bucketCache;
        this.properties = properties;
        this.redisLock = redisLock;
    }

    @Override
    public String getName() {
        return bucketCache.getName();
    }

    @Override
    public Object getNativeCache() {
        return this.bucketCache;
    }

    @Override
    public ValueWrapper get(Object key) {
        return null;
    }

    @Override
    public <T> T get(Object key, Class<T> type) {
        return null;
    }

    @Override
    public <T> T get(Object key, Callable<T> valueLoader) {
        return null;
    }

    @Override
    public void put(Object key, Object value) {

    }

    @Override
    public void evict(Object key) {

    }

    @Override
    public void clear() {

    }
}
