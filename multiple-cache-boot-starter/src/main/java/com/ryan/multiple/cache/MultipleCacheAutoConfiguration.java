/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache;

import com.ryan.multiple.cache.core.*;
import com.ryan.multiple.cache.properties.MultipleCacheProperties;
import com.ryan.multiple.cache.redis.RedisNamespaceHandler;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

/**
 * .多级缓存自动配置类
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
@Configuration
@EnableCaching
@EnableConfigurationProperties({MultipleCacheProperties.class})
public class MultipleCacheAutoConfiguration extends CachingConfigurerSupport  {

    @Autowired
    private MultipleCacheProperties multipleCacheProperties;

    @Autowired(required = false)
    @Qualifier("redissonClient") RedissonClient redissonClient;

    @Autowired(required = false)
    private RedisNamespaceHandler redisNamespaceHandler;

    @Bean
    public MultipleCacheNotifier cacheNotifier() {
        return Objects.isNull(redissonClient) ? new MultipleCacheNotifier()
                : new MultipleCacheNotifier(redissonClient.getTopic(getTopicName()));
    }

    @Bean
    public MultipleCacheSubscriber cacheSubscriber() {
        if (Objects.isNull(redissonClient)) {
            return new MultipleCacheSubscriber(cacheManager());
        }
        return new MultipleCacheSubscriber(redissonClient.getTopic(getTopicName()), cacheManager());
    }

    @Bean
    public MultipleCacheRemovalListener cacheRemovalListener() {
        return new MultipleCacheRemovalListener(cacheNotifier());
    }

    @Bean
    public MultipleCacheHelper cacheHelper() {
        MultipleCacheHelper cacheHelper = new MultipleCacheHelper();
        cacheHelper.setRedissonClient(redissonClient);
        cacheHelper.setNamespaceHandler(redisNamespaceHandler);
        cacheHelper.setCacheNotifier(cacheNotifier());
        cacheHelper.setRemovalListener(cacheRemovalListener());
        return cacheHelper;
    }

    @Bean
    @Override
    public MultipleCacheManager cacheManager() {
        MultipleCacheManager cacheManager = new MultipleCacheManager(multipleCacheProperties);
        cacheManager.setMultipleCacheHelper(cacheHelper());
        return cacheManager;
    }

    @Bean
    @Override
    public MultipleCacheResolver cacheResolver() {
        return new MultipleCacheResolver(cacheManager());
    }

    private String getTopicName() {
        return "multiple-cache-topic";
    }

}
