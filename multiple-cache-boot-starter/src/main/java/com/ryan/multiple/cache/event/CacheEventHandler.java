package com.ryan.multiple.cache.event;

import org.springframework.cache.Cache;

/**
 * .缓存事件处理器接口
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
public interface CacheEventHandler {

    /**
     * .缓存事件处理
     */
    boolean handle(Cache cache, Object cacheKey, Object cacheValue);

}
