/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.core;

import com.ryan.multiple.cache.event.CacheEventArgs;
import org.redisson.api.RTopic;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import javax.annotation.PostConstruct;
import java.util.Objects;

/**
 * .多级缓存订阅管理器
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
public class MultipleCacheSubscriber {

    public final RTopic topic;
    private final CacheManager cacheManager;
    public MultipleCacheSubscriber (RTopic topic, CacheManager cacheManager) {
        this.topic = topic;
        this.cacheManager = cacheManager;
    }

    public MultipleCacheSubscriber (CacheManager cacheManager) {
        this.topic = null;
        this.cacheManager = cacheManager;
    }

    @PostConstruct
    public void subscribe() {
        if (Objects.nonNull(topic)) {
            topic.addListener((channel, message) -> {
                if (message instanceof CacheEventArgs) {
                    CacheEventArgs args = (CacheEventArgs) message;
                    Cache cache = this.cacheManager.getCache(args.getCacheName());
                    MultipleCacheEventHandler.getInstance().handle(cache, args);
                }
            });
        }
    }


}
