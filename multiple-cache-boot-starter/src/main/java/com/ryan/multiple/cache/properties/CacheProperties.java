/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.properties;

import com.ryan.multiple.cache.consts.CacheType;

/**
 * .缓存配置项
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
public class CacheProperties {

    /**
     * .缓存名称
     */
    private String name;

    /**
     * .缓存类型：caffeine、redis、multiple
     */
    private String type;

    /**
     * .缓存存活时间（Time To Live）：是指缓存项从创建到过期的时间（单位：秒）
     */
    private Integer ttl;

    /**
     * .缓存最大空闲时间：是指缓存项在不被访问的情况下可以保持存活的最长时间（单位：秒）
     */
    private Integer maxIdleTime;

    /**
     * .最大缓存记录数
     */
    private Integer maxSize;

    /**
     * .是否允许有空值
     */
    private Boolean allowNullValues;

    /**
     * .数据存储类型：String, Hash, Set, ZSet, List
     */
    private String valueType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getTtl() {
        return ttl;
    }

    public void setTtl(Integer ttl) {
        this.ttl = ttl;
    }

    public Integer getMaxIdleTime() {
        return maxIdleTime;
    }

    public void setMaxIdleTime(Integer maxIdleTime) {
        this.maxIdleTime = maxIdleTime;
    }

    public Integer getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(Integer maxSize) {
        this.maxSize = maxSize;
    }

    public Boolean getAllowNullValues() {
        return allowNullValues;
    }

    public void setAllowNullValues(Boolean allowNullValues) {
        this.allowNullValues = allowNullValues;
    }

    public boolean isLocalCache() {
        return CacheType.CAFFEINE.name().equalsIgnoreCase(this.type);
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

}
