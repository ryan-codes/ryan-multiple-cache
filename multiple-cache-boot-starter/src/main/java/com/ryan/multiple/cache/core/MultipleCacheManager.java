/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.core;

import com.ryan.multiple.cache.properties.CacheProperties;
import com.ryan.multiple.cache.properties.MultipleCacheProperties;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractCacheManager;
import org.springframework.util.Assert;

import java.util.*;

/**
 * .多级缓存管理器类，通过继承AbstractCacheManager实现对Cache对象的管理
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
public class MultipleCacheManager extends AbstractCacheManager {

    private static final String DEFAULT_CACHE = "default";

    private MultipleCacheHelper multipleCacheHelper;

    private final MultipleCacheProperties multipleCacheProperties;
    private final CacheProperties defaultCacheProperties;
    public MultipleCacheManager(MultipleCacheProperties multipleCacheProperties) {
        this.multipleCacheProperties = multipleCacheProperties;
        this.defaultCacheProperties = multipleCacheProperties.getCache().remove(DEFAULT_CACHE);
        Assert.notNull(defaultCacheProperties, "Missing default cache properties!");
    }

    public void setMultipleCacheHelper(MultipleCacheHelper multipleCacheHelper) {
        this.multipleCacheHelper = multipleCacheHelper;
    }

    @Override
    protected Collection<? extends Cache> loadCaches() {
        Map<String, CacheProperties> cacheNamedMap = multipleCacheProperties.getCache();
        if (Objects.isNull(cacheNamedMap) || cacheNamedMap.isEmpty()) {
            return new ArrayList<>(0);
        }

        List<MultipleCache> multipleCaches = new ArrayList<>(cacheNamedMap.size());
        cacheNamedMap.forEach((cacheName, cache) -> {
            if (!DEFAULT_CACHE.equalsIgnoreCase(cacheName)) {
                cache.setName(cacheName);
                MultipleCache multipleCache = multipleCacheHelper.buildMultipleCache(cache);
                if (Objects.nonNull(multipleCache)) {
                    multipleCaches.add(multipleCache);
                }
            }
        });
        return multipleCaches;
    }

    @Override
    protected Cache getMissingCache(String cacheName) {
        Map<String, CacheProperties> cacheNamedMap = multipleCacheProperties.getCache();
        if (cacheNamedMap.containsKey(cacheName)) {
            CacheProperties cache = cacheNamedMap.get(cacheName);
            return multipleCacheHelper.buildMultipleCache(cache);
        }

        // 设置默认Cache
        CacheProperties cache = new CacheProperties();
        BeanUtils.copyProperties(defaultCacheProperties, cache);
        cache.setName(cacheName);
        return multipleCacheHelper.buildMultipleCache(cache);
    }

}
