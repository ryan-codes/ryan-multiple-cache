/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.redis;

import com.ryan.multiple.cache.redis.properties.LockProperties;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * .分布式锁业务类
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
public class RedisLockService {

    /**
     * .日志对象
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RedisLockService.class);

    private final LockProperties lockProperties;
    private final RedissonClient redissonClient;
    public RedisLockService(LockProperties lockProperties, RedissonClient redissonClient) {
        this.lockProperties = lockProperties;
        this.redissonClient = redissonClient;
    }

    /**
     * .获取分布式锁资源，并执行回调方法
     */
    public void tryLock(String lockName, Consumer<Boolean> consumer) {
        RLock rLock = redissonClient.getLock(lockName);
        Long waitTime = lockProperties.getWaitTime();
        Long leaseTime = lockProperties.getLeaseTime();
        try {
            if (Objects.nonNull(waitTime) && Objects.nonNull(leaseTime)) {
                if (rLock.tryLock(waitTime, leaseTime, TimeUnit.MILLISECONDS)){
                    consumer.accept(true);
                }
            } else if (Objects.nonNull(waitTime)) {
                if (rLock.tryLock(waitTime, TimeUnit.MILLISECONDS)){
                    consumer.accept(true);
                }
            } else {
                if (rLock.tryLock()){
                    consumer.accept(true);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Get distribute lock failed, lockName={}, waitTime={}, leaseTime={}.",
                    lockName, waitTime, leaseTime, ex);
        } finally {
            rLock.unlock();
        }
    }

}
