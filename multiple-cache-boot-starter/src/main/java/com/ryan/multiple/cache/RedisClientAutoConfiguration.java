/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache;

import com.ryan.multiple.cache.redis.RedisLockService;
import com.ryan.multiple.cache.redis.RedisServerConfigHandler;
import com.ryan.multiple.cache.redis.properties.RedisProperties;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * .Redis客户端自动配置类
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
@Configuration
@ConditionalOnClass({Redisson.class})
@EnableConfigurationProperties({RedisProperties.class})
public class RedisClientAutoConfiguration {

    @Autowired
    private RedisProperties redisProperties;

    @Autowired(required = false)
    private RedisServerConfigHandler redisServerConfigHandler;

    /**
     * 单机模式 redisson 客户端
     */
    @Bean("redissonClient")
    @ConditionalOnMissingBean(RedissonClient.class)
    @ConditionalOnProperty(name = "ryan.redis.mode", havingValue = "single")
    public RedissonClient redissonSingle() {
        Config config = new Config();
        config.setCodec(new JsonJacksonCodec());
        String address = redisProperties.getSingle().getAddress();
        SingleServerConfig serverConfig = config.useSingleServer()
                .setDatabase(redisProperties.getSingle().getDatabase())
                .setAddress(checkAddress(address));
        if (StringUtils.hasLength(redisProperties.getSingle().getPassword())) {
            serverConfig.setPassword(redisProperties.getSingle().getPassword());
        }
        if (Objects.nonNull(redisServerConfigHandler)) {
            redisServerConfigHandler.fillConfigs(serverConfig);
        }
        return Redisson.create(config);
    }


    /**
     * 主从模式 redisson 客户端
     */
    @Bean("redissonClient")
    @ConditionalOnMissingBean(RedissonClient.class)
    @ConditionalOnProperty(name = "ryan.redis.mode", havingValue = "masterSlave")
    public RedissonClient redissonMasterSlave() {
        Config config = new Config();
        config.setCodec(new JsonJacksonCodec());
        String master = redisProperties.getMasterSlave().getMaster();
        List<String> nodes = Stream.of(redisProperties.getMasterSlave().getNodes().split(","))
                .map(address -> checkAddress(address)).collect(Collectors.toList());
        MasterSlaveServersConfig serverConfig = config.useMasterSlaveServers()
                .setMasterAddress(checkAddress(master))
                .setDatabase(redisProperties.getMasterSlave().getDatabase())
                .addSlaveAddress(nodes.toArray(new String[nodes.size()]));
        if (StringUtils.hasLength(redisProperties.getSentinel().getPassword())) {
            serverConfig.setPassword(redisProperties.getSentinel().getPassword());
        }
        if (Objects.nonNull(redisServerConfigHandler)) {
            redisServerConfigHandler.fillConfigs(serverConfig);
        }
        return Redisson.create(config);
    }

    /**
     * 哨兵模式 redisson 客户端
     */
    @Bean("redissonClient")
    @ConditionalOnMissingBean(RedissonClient.class)
    @ConditionalOnProperty(name = "ryan.redis.mode", havingValue = "sentinel")
    public RedissonClient redissonSentinel() {
        Config config = new Config();
        config.setCodec(new JsonJacksonCodec());
        String master = redisProperties.getSentinel().getMaster();
        List<String> nodes = Stream.of(redisProperties.getSentinel().getNodes().split(","))
                .map(address -> checkAddress(address)).collect(Collectors.toList());
        SentinelServersConfig serverConfig = config.useSentinelServers()
                .setMasterName(checkAddress(master))
                .setDatabase(redisProperties.getSentinel().getDatabase())
                .addSentinelAddress(nodes.toArray(new String[nodes.size()]));
        if (StringUtils.hasLength(redisProperties.getSentinel().getPassword())) {
            serverConfig.setPassword(redisProperties.getSentinel().getPassword());
        }
        if (Objects.nonNull(redisServerConfigHandler)) {
            redisServerConfigHandler.fillConfigs(serverConfig);
        }
        return Redisson.create(config);
    }

    /**
     * 集群模式的 redisson 客户端
     */
    @Bean("redissonClient")
    @ConditionalOnMissingBean(RedissonClient.class)
    @ConditionalOnProperty(name = "ryan.redis.mode", havingValue = "cluster")
    public RedissonClient redissonCluster() {
        Config config = new Config();
        config.setCodec(new JsonJacksonCodec());
        List<String> nodes = Stream.of(redisProperties.getCluster().getNodes().split(","))
                .map(address -> checkAddress(address)).collect(Collectors.toList());
        ClusterServersConfig serverConfig = config.useClusterServers()
                .addNodeAddress(nodes.toArray(new String[nodes.size()]));
        if (StringUtils.hasLength(redisProperties.getCluster().getPassword())) {
            serverConfig.setPassword(redisProperties.getCluster().getPassword());
        }
        if (Objects.nonNull(redisServerConfigHandler)) {
            redisServerConfigHandler.fillConfigs(serverConfig);
        }
        return Redisson.create(config);
    }

    @Bean
    @ConditionalOnBean(value = RedissonClient.class)
    public RedisLockService redisLockService(@Autowired @Qualifier("redissonClient") RedissonClient redissonClient) {
        return new RedisLockService(redisProperties.getLock(), redissonClient);
    }

    private String checkAddress(String address) {
        if (!StringUtils.hasLength(address)) {
            throw new IllegalArgumentException("Redis server address can not be null or empty!");
        }
        return address.startsWith("redis://") ? address.trim() : "redis://" + address.trim();
    }

}
