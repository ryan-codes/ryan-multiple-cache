/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.consts;

import com.ryan.multiple.cache.core.MultipleCacheUtils;
import com.ryan.multiple.cache.datatype.RedissonListCache;
import com.ryan.multiple.cache.datatype.RedissonSetCache;
import com.ryan.multiple.cache.datatype.RedissonStringCache;
import com.ryan.multiple.cache.datatype.RedissonZSetCache;
import com.ryan.multiple.cache.properties.CacheProperties;
import org.redisson.api.*;
import org.redisson.spring.cache.CacheConfig;
import org.redisson.spring.cache.RedissonCache;
import org.springframework.cache.Cache;

import java.util.stream.Stream;

/**
 * .数据类型枚举类
 *
 * @author Ryanyu2016
 * @since 1.0.1
 */
public enum CacheValueType {

    /**
     * 字符串
     */
    STRING {
        @Override
        public Cache getCache(String cacheName, RedissonClient redissonClient, CacheProperties properties) {
            RBucket<Object> stringCache = redissonClient.getBucket(cacheName);
            RLock redisLock = redissonClient.getLock(cacheName);
            return new RedissonStringCache(cacheName, stringCache, redisLock, properties);
        }
    },

    /**
     * 哈希
     */
    HASH {
        @Override
        public Cache getCache(String cacheName, RedissonClient redissonClient, CacheProperties properties) {
            RMapCache<Object, Object> mapCache = redissonClient.getMapCache(cacheName);
            CacheConfig cacheConfig = MultipleCacheUtils.getCacheConfig(properties);
            return new RedissonCache(mapCache, cacheConfig, properties.getAllowNullValues());
        }
    },

    /**
     * 列表
     */
    LIST {
        @Override
        public Cache getCache(String cacheName, RedissonClient redissonClient, CacheProperties properties) {
            RList<Object> listCache = redissonClient.getList(cacheName);
            RLock redisLock = redissonClient.getLock(cacheName);
            return new RedissonListCache(cacheName, listCache, redisLock, properties);
        }
    },

    /**
     * 集合
     */
    SET {
        @Override
        public Cache getCache(String cacheName, RedissonClient redissonClient, CacheProperties properties) {
            RSetCache<Object> setCache = redissonClient.getSetCache(cacheName);
            return new RedissonSetCache(cacheName, setCache, properties);
        }
    },

    /**
     * 有序集合
     */
    ZSET {
        @Override
        public Cache getCache(String cacheName, RedissonClient redissonClient, CacheProperties properties) {
            RScoredSortedSet<Object> zSetCache = redissonClient.getScoredSortedSet(cacheName);
            return new RedissonZSetCache(cacheName, zSetCache, properties);
        }
    };

    public static CacheValueType of(String valueType) {
        return Stream.of(values()).filter(cacheValueType -> cacheValueType.name().equalsIgnoreCase(valueType)).findFirst().orElse(HASH);
    }

    public abstract Cache getCache(String cacheName, RedissonClient redissonClient, CacheProperties properties);

}
