/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ryan.multiple.cache.event;

/**
 * .缓存监听事件参数
 *
 * @author Ryanyu2016
 * @since 1.0.0
 */
public class CacheEventArgs {

    private CacheEvent command;

    private String cacheName;

    private Object cacheKey;

    private Object cacheValue;

    public CacheEvent getCommand() {
        return command;
    }

    public String getCacheName() {
        return cacheName;
    }

    public Object getCacheKey() {
        return cacheKey;
    }

    public Object getCacheValue() {
        return cacheValue;
    }

    @Override
    public String toString() {
        return "CacheEventArgs {" +
            " command=" + command.name() +
            ", cacheName='" + cacheName + '\'' +
            ", cacheKey=" + cacheKey +
            ", cacheValue=" + cacheValue +
        '}';
    }

    public static class Builder {

        private CacheEvent command;

        private String cacheName;

        private Object cacheKey;

        private Object cacheValue;

        public Builder command(CacheEvent command) {
            this.command = command;
            return this;
        }

        public Builder cacheName(String cacheName) {
            this.cacheName = cacheName;
            return this;
        }

        public Builder cacheKey(Object cacheKey) {
            this.cacheKey = cacheKey;
            return this;
        }

        public Builder cacheValue(Object cacheValue) {
            this.cacheValue = cacheValue;
            return this;
        }

        public CacheEventArgs build() {
            CacheEventArgs args = new CacheEventArgs();
            args.command = this.command;
            args.cacheName = this.cacheName;
            args.cacheKey = this.cacheKey;
            args.cacheValue = this.cacheValue;
            return args;
        }

    }

}
